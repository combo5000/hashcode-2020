package be.hashcode;

import java.util.List;

public class Pizza {
    private List<Pair<Long, Long>> pizzas;
    private final long maxSlices;
    private final long totalTypes;

    public Pizza(List<Pair<Long, Long>> pizzas, long maxSlices, long totalTypes) {
        this.pizzas = pizzas;
        this.maxSlices = maxSlices;
        this.totalTypes = totalTypes;
    }

    public void sortPizzas() {
        pizzas.sort((o1, o2) -> (int) (o2.getRight() - o1.getRight()));
    }

    public List<Pair<Long, Long>> getPizzas() {
        return pizzas;
    }

    public long getMaxSlices() {
        return maxSlices;
    }

    public long getTotalTypes() {
        return totalTypes;
    }
}
