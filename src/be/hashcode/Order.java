package be.hashcode;

import java.util.Collections;
import java.util.List;

class Order {
    private final long amount;
    private final List<Long> orders;

    public Order(long amount, List<Long> orders) {
        this.amount = amount;
        this.orders = orders;
        Collections.reverse(orders);
    }

    public long getAmount() {
        return amount;
    }

    public long getTypes() {
        return orders.size();
    }

    public List<Long> getOrders() {
        return orders;
    }

    @Override
    public String toString() {
        return String.format("types: %d, amount: %d\n%s", orders.size(), amount, orders.toString());
    }
}
