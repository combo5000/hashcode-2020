package be.hashcode.common;

import java.util.function.Supplier;

public class Performance {
    private Performance() {
    }

    public static void run(Runnable runnable, String name) {
        run(runnable, name, false);
    }

    public static void run(Runnable runnable, String name, boolean silent) {
        try {

            long t1 = System.nanoTime();
            runnable.run();
            long t2 = System.nanoTime();

            if (!silent) System.out.printf("%-20s: %f ms\n", name, (t2 - t1) / 1_000_000.0);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static <T> T run(Supplier<T> supp, String name) {
        try {
            long t1 = System.nanoTime();
            T val = supp.get();
            long t2 = System.nanoTime();
            System.out.printf("%-20s: %f ms\n", name, (t2 - t1) / 1_000_000.0);
            return val;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
