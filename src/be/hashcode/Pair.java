package be.hashcode;

import java.util.Objects;

public class Pair<T, T1> {
    private final T left;
    private final T1 right;

    public Pair(T left, T1 right) {
        this.left = left;
        this.right = right;
    }

    public T getLeft() {
        return left;
    }

    public T1 getRight() {
        return right;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pair<?, ?> pair = (Pair<?, ?>) o;
        return Objects.equals(left, pair.left) &&
                Objects.equals(right, pair.right);
    }

    @Override
    public int hashCode() {
        return Objects.hash(left, right);
    }

    @Override
    public String toString() {
        return String.format("pizza: %s with slices: %s", left, right);
    }
}
