package be.hashcode;

import be.gato.io.IOFactory;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

public class IOHelper {

    public static final String REGEX_SPACE = " ";

    protected static Pizza loadSet(String name) {
        Pizza pizza = null;
        try {
            Set<Pair<Long, Long>> pizzas = new HashSet<>();
            System.out.printf("Loading file %s\n", name);
            var blr = IOFactory.createBulkLinesReader(Paths.get(Objects.requireNonNull(Main.class.getClassLoader().getResource(name)).toURI()));
            final List<String> stringList = blr.read();
            String firstLine = stringList.get(0);
            final String[] maxAndTotal = firstLine.split(REGEX_SPACE);
            long max = Long.parseLong(maxAndTotal[0]);
            long total = Long.parseLong(maxAndTotal[1]);

            final String s = stringList.get(1);
            final String[] splitStrings = s.split(REGEX_SPACE);
            //TODO: create 4 lists Q1-Q4 to handle load evenly
            for (long i = 0; i < total; i++) {
                final String longAsString = splitStrings[(int) i];
                pizzas.add(new Pair<>(i, Long.parseLong(longAsString)));
            }
            System.out.printf("Loaded pizzas: %s\n\n", pizzas);
            pizza = new Pizza(new ArrayList<>(pizzas), max, total);
            return pizza;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return pizza;
    }

    protected static void writeSet(String name, Order order) {
        System.out.printf("Writing orders %s\n\n", order);
        try {
            var appender = IOFactory.createLineAppender(name, true);
            appender.append(order.getTypes());
            appender.append(order.getOrders(), ' ');
            appender.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
