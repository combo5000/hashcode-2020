package be.hashcode;

import be.hashcode.common.Performance;

import java.util.ArrayList;
import java.util.List;


public class Main {
    public static void main(String[] args) {
        Performance.run(Main::runSetA, "A example Completed in");
        Performance.run(Main::runSetB, "B small Completed in");
        Performance.run(Main::runSetC, "C medium Completed in");
        Performance.run(Main::runSetD, "d quite big Completed in");
        Performance.run(Main::runSetE, "e also big Completed in");
    }


    private static void runSetA() {
        Pizza pizzas = IOHelper.loadSet("a_example.in");
        final Order order = orderPizza(pizzas);
        // TODO: create new method to calc the scoring
        IOHelper.writeSet("a.out", order);
    }

    private static void runSetB() {
        Pizza pizzas = IOHelper.loadSet("b_small.in");
        final Order order = orderPizza(pizzas);
        IOHelper.writeSet("b.out", order);
    }

    private static void runSetC() {
        Pizza pizzas = IOHelper.loadSet("c_medium.in");
        final Order order = orderPizza(pizzas);
        IOHelper.writeSet("c.out", order);
    }

    private static void runSetD() {
        Pizza pizzas = IOHelper.loadSet("d_quite_big.in");
        final Order order = orderPizza(pizzas);
        IOHelper.writeSet("d.out", order);
    }

    private static void runSetE() {
        Pizza pizzas = IOHelper.loadSet("e_also_big.in");
        final Order order = orderPizza(pizzas);
        IOHelper.writeSet("e.out", order);
    }

    private static Order orderPizza(Pizza pizza) {
        pizza.sortPizzas();
        final List<Pair<Long, Long>> pizzaIds = pizza.getPizzas();
        List<Long> orders = new ArrayList<>();
        long amount = 0;
        //TODO: iterate over everything
        for (Pair<Long, Long> pair : pizzaIds) {
            if (calcAmountOfSlice(pizza, amount, pair)) {
                amount += pair.getRight();
                orders.add(pair.getLeft());
            }
        }
        return new Order(amount, orders);
    }

    private static boolean calcAmountOfSlice(Pizza pizza, long amount, Pair<Long, Long> pair) {
        return (amount + pair.getRight()) < pizza.getMaxSlices();
    }
}
